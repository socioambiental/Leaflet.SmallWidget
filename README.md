Leaflet.SmallWidget
===================

This plugins build a Leaflet Control skeleton with basic open/close functionality.

Example
-------

    L.Control.TestControl = L.Control.SmallWidget.extend({
      _icon: 'fa-test',
      _name: 'test-control',

      // Your custom content goes here
      render: function(map) {
        // Caption and select box
        var caption       = L.DomUtil.create('h3', '', this._container);
        caption.innerHTML = 'Just a caption';
      },

      // This is fired when the control is opened or closed
      customToggle: function () {
        if (L.DomUtil.hasClass(this._container, 'closed')) {
          // Custom code goes here
        }
      },
    });

    L.control.testcontrol = function(opts) {
      return new L.Control.TestControl(opts);
    }

    testControl = L.control.testcontrol({ position: 'topright' }).addTo(map);

About
-----

This code is heavilly based on [consbio/Leaflet.HtmlLegend: A simple Leaflet plugin for creating legends with HTML elements](https://github.com/consbio/Leaflet.HtmlLegend).
